from rest_framework import routers
from doctor.api import DoctorViewSet

router = routers.DefaultRouter()
router.register('api/doctor', DoctorViewSet, 'doctor')

urlpatterns = router.urls
