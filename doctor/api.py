from doctor.models import Doctor
from doctor.serializers import DoctorSerializer
from rest_framework import viewsets, permissions

class DoctorViewSet(viewsets.ModelViewSet):
    queryset = Doctor.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = DoctorSerializer